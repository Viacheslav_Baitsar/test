package Collection;

import java.util.ArrayList;
import java.util.List;

class OrderList {

    private List<Order> orders;

    public OrderList() {
        this.orders = new ArrayList<Order>();
    }

    public void AddOrder(Order order) {
        this.orders.add(order);
    }

    public OrderList(List<Order> orders) {
        this.orders = orders;
    }

    public List<Order> getOrders() {
        return List.copyOf(orders);
    }




}
